package main;

/**
 * The {@link Main} class has the entry point of the Triangle Challenge program that takes a list of arguments,
 * parse them and passes arguments to the {@link TriangleChallenge} class for processing. The result is displayed
 * to the STD output.
 *
 * @author maxim domentii
 */
public class Main {

    public static void main(String[] args) {
        int[] input = readInput(args);

        TriangleType result = TriangleChallenge.getTriangleType(input[0], input[1], input[2]);

        System.out.println(result);
    }

    private static int[] readInput(String[] args){
        if (args.length != 3){
            throw new IllegalArgumentException("Invalid length of the arguments array. Expected length is 3");
        }
        int[] input = new int[3];
        for (int i=0; i<3; i++){
            try {
                input[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("NumberFormatException when parsing input " + args[i]);
            }
        }
        return input;
    }
}
