package main;

/**
 * The {@link TriangleChallenge} class has a static method getTriangleType() that take the lengths of the triangle's
 * three sides as input, and return the {@link TriangleType}.
 *
 * @author maxim domentii
 */
public class TriangleChallenge {

    /**
     * Take the lengths of the triangle's three sides as input, and return the type of triangle.
     *
     * @param a first side length
     * @param b second side length
     * @param c third sie length
     * @return type of triangle
     */
    public static TriangleType getTriangleType(int a, int b, int c){
        if (a+b <= c || a+c <= b || b+c <= a){
            throw new IllegalArgumentException("Given inputs are not valid lengths of a triangle");
        }

        if (a == b  && a == c){
            return TriangleType.EQUILATERAL;
        } else if (a == b || a == c || b == c){
            return TriangleType.ISOSCELES;
        } else {
            return TriangleType.SCALENE;
        }
    }
}
