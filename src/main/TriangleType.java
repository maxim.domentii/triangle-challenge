package main;

/**
 * The {@link TriangleType} enum class represent the list of triangle possible types
 *
 * @author maxim domentii
 */
public enum TriangleType {
    EQUILATERAL,
    ISOSCELES,
    SCALENE
}
