package test;

import main.Main;
import main.TriangleChallenge;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

public class MainTest {

    @Test(expected = IllegalArgumentException.class)
    public void testMainException1() {
        Main.main(new String[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainException2() {
        Main.main(new String[]{"1", "1"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainException3() {
        Main.main(new String[]{"1", "1", "1", "1"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMainException4() {
        Main.main(new String[]{"a", "1", "1"});
    }

    @Test
    public void testMain() {
        Main.main(new String[]{"1", "1", "1"});
        PowerMockito.verifyStatic(TriangleChallenge.class, Mockito.times(1));
    }
}
