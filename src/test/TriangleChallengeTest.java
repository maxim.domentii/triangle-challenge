package test;

import main.TriangleChallenge;
import main.TriangleType;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TriangleChallengeTest {

    @Test(expected = IllegalArgumentException.class)
    public void testGetTriangleTypeException1(){
        TriangleChallenge.getTriangleType(1, 1, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTriangleTypeException2(){
        TriangleChallenge.getTriangleType(1, 3, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTriangleTypeException3(){
        TriangleChallenge.getTriangleType(3, 1, 1);
    }

    @Test
    public void testGetTriangleType(){
        assertEquals(TriangleType.EQUILATERAL, TriangleChallenge.getTriangleType(1, 1, 1));
        assertEquals(TriangleType.ISOSCELES, TriangleChallenge.getTriangleType(2, 2, 3));
        assertEquals(TriangleType.SCALENE, TriangleChallenge.getTriangleType(2, 4, 5));
    }
}
